package service;

import java.util.ArrayList;
import java.util.List;

public class Mystery {
    private final List<Other> stuff = new ArrayList<>();

    public void addOther(Other other) {
        other.addOther(other);
    }

    public void removeOther(Other other) {
        other.remove(other);
    }

    private void notifyOther() {
        for (var other : stuff) {
            other.call("context");
        }
    }
}
