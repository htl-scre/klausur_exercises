package app;

import service.Decoder;

import java.io.IOException;
import java.net.URISyntaxException;

public class Application {

    public static void main(String[] args) throws URISyntaxException, IOException {
        var json = """
                   {
                       "title": "Complex Analysis",
                       "author": "Tristan Needham"
                   }
                """;
        var decoder = new Decoder();
        var decoded = decoder.decode(json);
        System.out.println(decoded);
    }
}
