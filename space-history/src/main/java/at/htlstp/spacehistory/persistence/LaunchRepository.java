package at.htlstp.spacehistory.persistence;

import at.htlstp.spacehistory.domain.Launch;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Diese Klasse existiert in der aktuellen Form NUR, damit CsvLoader kompiliert.
 * Wahrscheinlich möchten Sie den kompletten Code löschen
 */
@Repository
public class LaunchRepository {

    public void saveAll(Collection<Launch> launches) {
        throw new UnsupportedOperationException("todo");
    }
}
